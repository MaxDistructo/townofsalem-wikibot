package maxdistructo.tosbot.legacy

import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.jda.message.Messages
import maxdistructo.tosbot.Utils

object FAQs {
    class Command : com.jagrosh.jdautilities.command.Command(){
        init{
            this.name = "faq"
            this.guildOnly = false
        }

        override fun execute(event: CommandEvent?) {
            val message = event!!.message!!
            val args = Utils.getArgs(message)
            Messages.sendMessage(event.textChannel, getFAQs(args[1]))
        }

    }
    fun getFAQs(role: String) : String
    {
        return when(role){
            "transporter" -> "You cannot move the following actions: Jester's Guilt, Guardian Angel's Defend, Vigilante's Suicide, SK/WW kill on Roleblockers, Veteran/Medusa kills on visitors, Arsonist ignites, Hex Master's unstoppable attack, Bodyguard's kill, WW/Pest/Jugg's Rampage Abilities, Crus/Ambusher's Attacks\n" +
                             "You are immune to being controlled\n"+
                             "You cannot be roleblocked by an Escort, Consort, or Pirate\n"+
                             "You cannot transport a person with themselves\n"+
                             "If you and another trans choose to target the same person, whoever joined the lobby first has priority\n"+
                             "You cannot transport a pirate duel's target, but you can transport the pirate's attack if they lose the duel.\n"+
                             "You cannot transport a jailed person\n"+
                             "If you end up making a person target themselves, they will use their night ability on themselves if they cannot usually visit themselves\n"+
                             "If you end up making a person target themselves who can usually target themselves, they will use their self target action\n"+
                             "If two Transporters select the same person, then the outcome will depend on the order in which the Transporters appeared in the lobby before the game started. Let T1 and T2 be Transporters, and let T1 join the lobby before T2. The game will process their targets' transportation thus:\n" +
                    "\t 1. T1 marks their left target for transportation;\n" +
                    "\t 2. If T1's right target is the same as T2's right target, then T2 marks their right target for transportation;\n" +
                    "\t 3. T1 swaps whoever they marked with their right target;\n" +
                    "\t 4. If T2 didn't mark anyone in step 2, then they mark whoever is at their right target's house;\n" +
                    "\t 5. T2 swaps whoever is at their left target's house with the person they marked."
            "sheriff" -> "If a person is found to be not sus or innocent, they may be the Godfather or most coven members with the Necronomicon"
            "spy" -> "You cannot see the visits of mafia members the Disguiser hides\n If your target is Hypnoed, you will see the hypnoed result as well."
            "lookout" -> "You cannot see the visits of the following: Any role targeting themselves besides transporter, Any role that attacked who your target visited, Godfather, Witch/Coven Leader's second target, Hex Master (with Necro), Retributionist/Necromancer, Guardian Angel, Arsonist's ignite"
            "bodyguard" -> "You can defend the following attacks: Vigilante, Vampire Hunter, Godfather, Mafioso, Vampire, Necromancer's Ghoul, Coven Leader (with necro), Potion Master, Medusa (with necro), Poisoner, Pestilence, Pirate(when winning duel), and any NK besides Arsonist.\n If you defend from the Necromancer's Ghoul, you will still die and the Necromancer will be fine. \n If a Juggernaut attacks your target, you, your target, and the Juggernaut will die along with everyone else that would normally die from the Juggernaut.\n You will not kill the Pestilence but will still sucessfully defend your target and die.\n You cannot defend from: Jailor, Veteran, other TPs, Ambusher, Medusa, Hex Master, Arsonists, and Jesters. \n You can defend the Jailor from them Jailing a WW or SK, killing the WW or SK. \n Bodyguard defends will stack with multiple bodyguards\n Each bodyguard can only defend 1 attack \n You cannot save another bodyguard from themselves"
            "doctor" -> "You cannot save people from the following: Poisoner (with necro), Juggernaut, Arsonist, Hex Master's final hex, A broken heart, Vigilante's Suicide, Jester's haunt, Mayor (once revealed), Jailor, or remove Douses, Hexes, or Plague.\n You will heal ALL attacks on your target that you can heal them from."
            "escort" -> "You cannot roleblock the following roles: Witch, Coven Leader, Pestilence, Veterans, Escort, Transporter, Retributionist, Necromancer, Pirate, and Consort \n In a potential stalemate situation, you will always lose to your target."
            "mayor" -> "Once revealed, you cannot be healed by a Doctor or be whispered to\n In a 1v1 situation against any evil, you are able to alone put them up and lynch them"
            "retributionist" -> "You cannot revive the following: Psychic, Trapper, Jailor, Veteran, Mayor, Transporter, or another Retributionist\nYou cannot revive the same player as a Necromancer, and only you or the necromancer can use each body once.\nIf you are controlled, that corpse will not be wasted"
            else -> "No additional FAQs are avaliable for this role"
        }
    }
}