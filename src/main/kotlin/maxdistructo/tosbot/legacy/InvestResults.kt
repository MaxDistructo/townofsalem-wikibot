package maxdistructo.tosbot.legacy

import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.jda.message.Messages
import maxdistructo.tosbot.ToSBot
import maxdistructo.tosbot.Utils
import net.dv8tion.jda.api.EmbedBuilder
import java.time.Instant

object InvestResults {

    class Command : com.jagrosh.jdautilities.command.Command()
    {
        init{
            this.name = "investresutlts"
            this.aliases = arrayOf("ir", "results")
            this.guildOnly = false
        }
        override fun execute(event: CommandEvent?) {
            val message = event!!.message!!
            val args = Utils.getArgs(message)
            try{
                val builder = EmbedBuilder()
                builder.setDescription(getResults(args[1], args[2]))
                builder.setFooter(ToSBot.bot.client.selfUser.name, ToSBot.bot.client.selfUser.effectiveAvatarUrl)
                builder.setTimestamp(Instant.now())
                builder.setAuthor("Investigator Results for ${Utils.fixName(args[1])}")
                Messages.sendMessage(event.textChannel, builder.build())
            }
            catch(e: IndexOutOfBoundsException){
                val builder = EmbedBuilder()
                builder.setDescription(getResults(args[1], "coven"))
                builder.setFooter(ToSBot.bot.client.selfUser.name, ToSBot.bot.client.selfUser.effectiveAvatarUrl)
                builder.setTimestamp(Instant.now())
                builder.setAuthor("Investigator Results for ${Utils.fixName(args[1])}")
                Messages.sendMessage(event.textChannel, builder.build())
            }
        }
    }

    fun getResults(role: String, gamemode: String) : String
    {
        if(gamemode == "classic") {
            if (role == "investigator" || role == "consigliere" || role == "mayor" )
            {
                return "Investigator, Consigliere, Mayor"
            }
            if(role == "vigilante" || role == "veteran" || role == "mafioso" || role == "ambusher")
            {
                return "Vigilante, Veteran, Mafioso, Ambusher"
            }
            if(role == "medium" || role == "janitor" || role == "retributionist")
            {
                return "Medium, Janitor, Retributionist"
            }
            if(role == "survivor" || role == "vampire_hunter" || role == "amnesiac")
            {
                return "Survivor, Vampire Hunter, Amnesiac"
            }
            if(role == "spy" || role == "blackmailer" || role == "jailor")
            {
                return "Spy, Blackmailer, Jailor"
            }
            if(role == "sheriff" || role == "executioner" || role == "werewolf")
            {
                return "Sheriff, Executioner, Werewolf"
            }
            if(role == "framer" || role == "vampire" || role == "jester")
            {
                return "Framer, Vampire, Jester"
            }
            if(role == "lookout" || role == "forger" || role == "witch")
            {
                return "Lookout, Forger, Witch"
            }
            if(role == "escort" || role == "transporter" || role == "consort" || role == "hypnotist")
            {
                return "Escort, Transporter, Consort, Hypnotist"
            }
            if(role == "doctor" || role == "disguiser" || role == "serial_killer")
            {
                return "Doctor, Disguiser, Serial Killer"
            }
            if(role == "bodyguard" || role == "godfather" || role == "arsonist")
            {
                return "Bodyguard, Godfather, Arsonist"
            }
            if(role == "all")
            {
                val roles = mutableListOf("investigator", "vigilante", "medium", "survivor", "spy", "sheriff", "framer", "lookout", "escort", "doctor", "bodyguard")
                var results = ""
                for(srole in roles)
                {
                    if(srole != "bodyguard") {
                        results += getResults(srole, gamemode) + "\n\n"
                    }
                    else{
                        results += getResults(srole, gamemode)
                    }
                }
                return results
            }
        }
        else if(gamemode == "coven")
        {
            if (role == "investigator" || role == "consigliere" || role == "mayor" || role == "trapper" || role == "plaguebearer")
            {
                return "Investigator, Consigliere, Mayor, Tracker, or Plaguebearer"
            }
            if(role == "vigilante" || role == "veteran" || role == "mafioso" || role == "ambusher" || role == "pirate")
            {
                return "Vigilante, Veteran, Mafioso, Pirate, or Ambusher"
            }
            if(role == "medium" || role == "janitor" || role == "retributionist" || role == "necromancer" || role == "trapper")
            {
                return "Medium, Janitor, Retributionist, Necromancer, or Trapper"
            }
            if(role == "survivor" || role == "vampire_hunter" || role == "amnesiac" || role == "medusa" || role == "psychic")
            {
                return "Survivor, Vampire Hunter, Amnesiac, Medusa, or Psychic"
            }
            if(role == "spy" || role == "blackmailer" || role == "jailor" || role == "guardian_angel")
            {
                return "Spy, Blackmailer, Jailor, or Guardian Angel"
            }
            if(role == "sheriff" || role == "executioner" || role == "werewolf" || role == "poisoner")
            {
                return "Sheriff, Executioner, Werewolf, or Poisoner"
            }
            if(role == "framer" || role == "vampire" || role == "jester" || role == "hex_master")
            {
                return "Framer, Vampire, Jester, or Hex Master"
            }
            if(role == "lookout" || role == "forger" || role == "coven_leader" || role == "juggernaut")
            {
                return "Lookout, Forger, Juggernaut, or Coven Leader"
            }
            if(role == "escort" || role == "transporter" || role == "consort" || role == "hypnotist")
            {
                return "Escort, Transporter, Consort, Hypnotist"
            }
            if(role == "doctor" || role == "disguiser" || role == "serial_killer" || role == "potion_master")
            {
                return "Doctor, Disguiser, Serial Killer, or Potion Master"
            }
            if(role == "bodyguard" || role == "godfather" || role == "arsonist" || role == "crusader")
            {
                return "Bodyguard, Godfather, Arsonist, or Crusader"
            }
            if(role == "all")
            {
                val roles = mutableListOf("investigator", "vigilante", "medium", "survivor", "spy", "sheriff", "framer", "lookout", "escort", "doctor", "bodyguard")
                var results = ""
                for(srole in roles)
                {
                    if(srole != "bodyguard") {
                        results += getResults(srole, gamemode) + "\n\n"
                    }
                    else{
                        results += getResults(srole, gamemode)
                    }
                }
                return results
            }
        }
        else{
            return "Provided gamemode is invalid"
        }
        return "Error in pulling invest results"
    }
}