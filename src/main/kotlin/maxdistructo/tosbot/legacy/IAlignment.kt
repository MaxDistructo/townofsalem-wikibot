package maxdistructo.tosbot.legacy

import java.awt.Color

interface IAlignment {
    val regName: String
    val displayName: String
    val color: Color
    val allies: String
    val mustKill: String
}
