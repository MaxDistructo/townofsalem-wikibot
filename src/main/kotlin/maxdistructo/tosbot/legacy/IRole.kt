package maxdistructo.tosbot.legacy

import java.awt.Color

interface IRole {
    val regName : String
    val isUnique: Boolean
    val classification : String
    val alignment : IAlignment
    val summary : String
    val description : String
    val thumbnail : String
    val icon : String
    val attack : Int
    val defence : Int
    fun getColor(): Color
}
