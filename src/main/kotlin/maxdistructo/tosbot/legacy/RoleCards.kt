package maxdistructo.tosbot.legacy

import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.jda.message.Messages
import maxdistructo.tosbot.ToSBot
import maxdistructo.tosbot.Utils
import java.awt.Color
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import java.time.Instant

object RoleCards {
    class Command : com.jagrosh.jdautilities.command.Command(){
        init{
            this.name = "rolecard"
            this.aliases = arrayOf("rc")
            this.guildOnly = false
        }
        override fun execute(event: CommandEvent?) {
            val message = event!!.message!!
            val args = Utils.getArgs(message)
            Messages.sendMessage(event.textChannel, onRoleCardAsk(args[1]))
        }

    }
    fun onRoleCardAsk(role : String) : MessageEmbed{
        return onRoleCardAsk(null, role, null)
    }
    fun onRoleCardAsk(message: Message?, role: String, user: Member?): MessageEmbed {
        val builder = EmbedBuilder()
        builder.setFooter(ToSBot.bot.client.selfUser.name, ToSBot.bot.client.selfUser.avatarUrl)
        builder.setTimestamp(Instant.now())

        val townColor = Color( 0x6BC617)
        val mafiaColor = Color(0xDF0205)
        val amneColor = Color(0x1BE8DD)
        val survColor = Color(0xCACC55)
        val vampColor = Color(0x7B8969)
        val arsoColor = Color(0xEB8211)
        val skColor = Color(0x0A0984)
        val wwColor = Color(0x825037)
        val witchColor = Color(0xC15FFF)
        val gaColor = Color(0xEBF5F1)
        val juggColor = Color(0x801E4B)
        val pirateColor = Color(0xDDAE61)
        val plagueColor = Color(0xCEFB63)
        val pestColor = Color(0x101212)

        when (role.toLowerCase()) { //Fix a few player mistypes
            "investigator" -> {
                builder.addField("Class", "Town Investigative (TI)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A private eye who secretly gathers information.", false)
                builder.setAuthor(
                    "Investigator",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/2/20/Achievement_Investigator.png/revision/latest/scale-to-width-down/50?cb=20140825150920"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/c/cb/Investigator.png/revision/latest?cb=20141222203926")
                builder.setDescription("Investigate one person each night for a clue to their role.")
                builder.setColor(townColor)

            }
            "sheriff" -> {
                builder.addField("Class", "Town Investigative (TI)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField(
                    "Summary",
                    "The law enforcer of the Town, forced into hiding from threat of murder.",
                    false
                );
                builder.setAuthor(
                    "Sheriff",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e7/Achievement_Sheriff.png/revision/latest/scale-to-width-down/50?cb=20140825150706"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/3/3e/Sheriff.png/revision/latest/scale-to-width-down/150?cb=20140802032529")
                builder.setDescription("Interrogate one person each night for suspicious activity.")
                builder.setColor(townColor)

            }
            "spy" -> {
                builder.addField("Class", "Town Investigative (TI)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A talented watcher who keeps track of evil in the Town.", false);
                builder.setAuthor(
                    "Spy",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/0/0b/Achievement_Spy.png/revision/latest/scale-to-width-down/50?cb=20140825150715"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/3/36/Spy.png/revision/latest/scale-to-width-down/150?cb=20151129195522")
                builder.setDescription("Each night you will be able to bug a town member to see what occurs to them. You will also know who the mafia and coven visit.")
                builder.setColor(townColor)
            }
            "lookout" -> {

                builder.addField("Class", "Town Investigative (TI)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField(
                    "Summary",
                    "An eagle-eyed observer, stealthily camping outside houses to gain information.",
                    false
                );
                builder.setAuthor(
                    "Lookout",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f6/Achievement_Lookout.png/revision/latest/scale-to-width-down/50?cb=20140825150631"
                )
                //builder.setThumbnail("https://town-of-salem.fandom.com/wiki/$role")
                builder.setDescription("Watch one person at night to see who visits them. You will only know up to 3 of the people who visit them. If there is more visitors, you will be told but not know who they are.")
                builder.setColor(townColor)

            }
            "vigilante" -> {

                builder.addField("Class", "Town Killing (TK)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A militant officer willing to bend the law to enact justice.", false);
                builder.setAuthor(
                    "Vigilante",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/1/1f/Achievement_Vigilante.png/revision/latest?cb=20140825150808"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/3/3c/Brock_Smith.png/revision/latest/scale-to-width-down/138?cb=20160914033426")
                builder.setDescription("Choose to take justice into your own hands and shoot someone. You only have 3 shots and if you kill another town member, you will suicide.")
                builder.setColor(townColor)

            }
            "veteran" -> {

                builder.addField("Class", "Town Killing (TK)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 2 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A paranoid war hero who will shoot anyone who visits him.", false);
                builder.setAuthor(
                    "Veteran",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/1/1b/Achievement_Veteran.png/revision/latest/scale-to-width-down/50?cb=20140825150759"
                )
                builder.setDescription("Decide if you will go on alert. You can go on alert 3 times killing all who visit you. When you are on alert, you will have a defence of 1 or general immunity.")
                builder.setColor(townColor)
            }
            "vampire_hunter" -> {

                builder.addField("Class", "Town Killing (TK)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A priest turned monster hunter, this person slays Vampires.", false);
                builder.setAuthor(
                    "Vampire Hunter",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/8/8e/Achievement_Vampire_Hunter.png/revision/latest/scale-to-width-down/50?cb=20151130210939"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/c/c8/Vampire_Hunter.png/revision/latest/scale-to-width-down/150?cb=20151101133904")
                builder.setDescription("Check for Vampires each night. If you find a Vampire you will attack them. If a Vampire visits you, you will attack them. You can hear Vampires at night. If all the Vampires die you will become a Vigilante.")
                builder.setColor(townColor)

            }
            "jailor" -> {

                builder.addField("Class", "Town Killing (TK)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 2 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A prison guard who secretly detains suspects.", false)
                builder.setAuthor(
                    "Jailor",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/6/64/Achievement_Jailor.png/revision/latest/scale-to-width-down/50?cb=20140825150602"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/7/7e/Jailor.png/revision/latest/scale-to-width-down/150?cb=20151021224315")
                builder.setDescription("You may choose one person during the day to jail for the night. You may anonymously talk with your prisoner. You can choose to attack your prisoner. The jailed target can't perform their night ability.")
                builder.setColor(townColor)
            }
            "bodyguard" -> {

                builder.addField("Class", "Town Protective (TP)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 2 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "An ex-soldier who secretly makes a living by selling protection.", false)
                builder.setAuthor(
                    "Bodyguard",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/e/ef/Achievement_Bodyguard.png/revision/latest/scale-to-width-down/50?cb=20140708090613"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/8/87/Bodyguard.png/revision/latest?cb=20150207170237")
                builder.setDescription("Protect a player from direct attacks at night. If your target is attacked or is the victim of a harmful visit, you and the visitor will fight. If you successfully protect someone you can still be Healed.")
                builder.setColor(townColor)
            }
            "doctor" -> {

                builder.addField("Class", "Town Protective (TP)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A surgeon skilled in trauma care who secretly heals people.", false)
                builder.setAuthor(
                    "Doctor",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/a/a0/Achievement_Doctor.png/revision/latest/scale-to-width-down/50?cb=20140708093156"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/6/6c/Doctor.png/revision/latest?cb=20140802032512")
                builder.setDescription("Heal one person each night, preventing them from dying. You may only Heal yourself once. You will know if your target is attacked.")
                builder.setColor(townColor)
            }
            "escort" -> {

                builder.addField("Class", "Town Support (TS)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A beautiful woman skilled in distraction.", false)
                builder.setAuthor(
                    "Escort",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/2/29/Achievement_Escort.png/revision/latest/scale-to-width-down/50?cb=20140708093747"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/d/d3/Escort.png/revision/latest?cb=20150506224645")
                builder.setDescription("Distract someone each night. Distraction blocks your target from using their role's night ability. You cannot be role blocked.")
                builder.setColor(townColor)
            }
            "mayor" -> {

                builder.addField("Class", "Town Support (TS)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "The leader of the Town.", false)
                builder.setAuthor(
                    "Mayor",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/a/aa/Achievement_Mayor_2017.png/revision/latest/scale-to-width-down/50?cb=20171130202502"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/8/8b/MoneyBag.png/revision/latest?cb=20141029221203")
                builder.setDescription("You may reveal yourself as the Mayor of the Town. Once you have revealed yourself as Mayor your vote counts as 3 votes. You may not be healed once you have revealed yourself. Once revealed, you can't whisper or be whispered to.")
                builder.setColor(townColor)
            }
            "medium" -> {

                builder.addField("Class", "Town Support (TS)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A secret Psychic who talks with the dead.", false)
                builder.setAuthor(
                    "Medium",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/6/63/Achievement_Medium.png/revision/latest/scale-to-width-down/50?cb=20140825150649"
                )
                builder.setThumbnail("https://i.imgur.com/WBTx4Kx.png")
                builder.setDescription("You can speak to the dead and they can speak to you. All messages you send to the dead are anonymous. Once you die, you are able to tell the town one message.")
                builder.setColor(townColor)
            }
            "retributionist" -> {

                builder.addField("Class", "Town Support (TS)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField(
                    "Summary",
                    "A powerful mystic who will give one person a second chance at life.",
                    false
                )
                builder.setAuthor(
                    "Retributionist",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/6/61/Achievement_Retributionist.png/revision/latest/scale-to-width-down/50?cb=20140825150657"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/Nightspirit174%27s_Retributionist_Avatar.png/revision/latest?cb=20160423042204")
                builder.setDescription("You may call upon each dead town member once, using their ability as your own for that night.")
                builder.setColor(townColor)
            }
            "transporter" -> {

                builder.addField("Class", "Town Support (TS)", true)
                builder.addField("Goal", "Lynch every criminal or evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, and other Town members",
                    false
                )
                builder.addField("Summary", "A man who transports people without asking any questions.", false)
                builder.setAuthor(
                    "Transporter",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/5/5a/Achievement_Transporter.png/revision/latest/scale-to-width-down/50?cb=20140825150750"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/Nightspirit174%27s_Retributionist_Avatar.png/revision/latest?cb=20160423042204")
                builder.setDescription("Choose two people to transport at night. Transporting two people swaps all targets against them. You may transport yourself. Your targets will know they were transported. You may not transport someone with themselves. You can not transport Jailed people.")
                builder.setColor(townColor)
            }
            "blackmailer" -> {

                builder.addField("Class", "Mafia Support (MS)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "An eavesdropper who uses information to keep people quiet.", false)
                builder.setAuthor(
                    "Blackmailer",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/0/0f/Achievement_Blackmailer.png/revision/latest?cb=20140825150350"
                )
                builder.setThumbnail("https://orig00.deviantart.net/3959/f/2016/043/f/0/blackmailer_skin_5_by_purplellamas_town_of_salem_by_purplellamas5-d9ri8cf.png")
                builder.setDescription("Choose one person each night to blackmail. Blackmailed targets can not talk during the day. You can hear private messages.")
                builder.setColor(mafiaColor)
            }
            "consigliere" -> {

                builder.addField("Class", "Mafia Support (MS)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A corrupted Investigator who gathers information for the Mafia.", false)
                builder.setAuthor(
                    "Consigliere",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f6/Achievement_Consigliere.png/revision/latest?cb=20140825150405"
                )
                builder.setThumbnail("https://orig00.deviantart.net/3959/f/2016/043/f/0/blackmailer_skin_5_by_purplellamas_town_of_salem_by_purplellamas5-d9ri8cf.png")
                builder.setDescription("Check one person for their exact role each night.")
                builder.setColor(mafiaColor)

            }
            "consort" -> {

                builder.addField("Class", "Mafia Support (MS)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A beautiful dancer working for organized crime.", false)
                builder.setAuthor(
                    "Consort",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f4/Achievement_Consort.png/revision/latest/scale-to-width-down/50?cb=20140825150414"
                )
                builder.setThumbnail("http://town-of-salem.wikia.com/wiki/File:Escort.png")
                builder.setDescription("Distract someone each night. Distraction blocks your target from using their role's night ability.")
                builder.setColor(mafiaColor)

            }
            "disguiser" -> {

                builder.addField("Class", "Mafia Deception (MD)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A master of disguise who pretends to be other roles.", false)
                builder.setAuthor(
                    "Disguiser",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/b/bf/Achievement_Disguiser.png/revision/latest/scale-to-width-down/50?cb=20140825150509"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/e/e4/NasubiNori_Disguiser_Avatar.png/revision/latest?cb=20180311074829")
                builder.setDescription("Disguise the visit of a fellow mafia member to be that of another player. They will also be seen to investigators as the role you disguised them as. Spies will see your visits to fellow mafia members.")
                builder.setColor(mafiaColor)

            }
            "forger" -> {

                builder.addField("Class", "Mafia Deception (MD)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A crooked lawyer that replaces documents.", false)
                builder.setAuthor(
                    "Forger",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f0/Achievement_Forger.png/revision/latest/scale-to-width-down/50?cb=20150801143107"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/ForgerAvatar.png/revision/latest?cb=20150724030632")
                builder.setDescription("You may change the role and will that someone who is killed will show up as. This ability has only 2 uses.")

                builder.setColor(mafiaColor)
            }
            "framer" -> {

                builder.addField("Class", "Mafia Deception (MD)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A skilled counterfeiter who manipulates information.", false)
                builder.setAuthor(
                    "Framer",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/5/5c/Achievement_Framer.png/revision/latest/scale-to-width-down/50?cb=20140825150526"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/ForgerAvatar.png/revision/latest?cb=20150724030632")
                builder.setDescription("Choose someone to frame at night. If your target is investigated they will appear to be a member of the Mafia. Frames last until that person is investigated.")
                builder.setColor(mafiaColor)

            }
            "janitor" -> {

                builder.addField("Class", "Mafia Deception (MD)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A sanitation expert working for organized crime.", false)
                builder.setAuthor(
                    "Janitor",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/1/1a/Achievement_Janitor.png/revision/latest/scale-to-width-down/50?cb=20140825150612"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/4/4d/Yuan_Itor.png/revision/latest/scale-to-width-down/154?cb=20160826042531")
                builder.setDescription("Choose a person to clean at night. If they die, their role will not be revealed to the town. You will know their role though. You only have 3 uses of this ability.")
                builder.setColor(mafiaColor)

            }
            "ambusher" -> {
                builder.addField("Class", "Mafia Killing (MK)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to the Mafia.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField(
                    "Summary",
                    "A stealthy killer who lies in wait for the perfect moment to strike.",
                    false
                )
                builder.setAuthor(
                    "Ambusher",
                    "https://static.wikia.nocookie.net/town-of-salem/images/7/7e/RoleIcon_Ambusher.png/revision/latest/scale-to-width-down/100?cb=20200910205812",
                    "https://static.wikia.nocookie.net/town-of-salem/images/7/7e/RoleIcon_Ambusher.png/revision/latest/scale-to-width-down/100?cb=20200910205812"
                )
                //builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/4/4d/Yuan_Itor.png/revision/latest/scale-to-width-down/154?cb=20160826042531")
                builder.setDescription("Choose a person to watch over. You will randomly kill one of their visitors. Any living visitors will know who you are.")
                builder.setColor(mafiaColor)
            }
            "hypnotist" -> {
                builder.addField("Class", "Mafia Killing (MK)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to the Mafia.", true)
                builder.addField("Attack: 0 - Defence: 0", "", false)
                builder.addField(
                    "Summary", "" +
                            "A skilled hypnotist who can alter the perception of others.", false
                )
                builder.setAuthor(
                    "Hypnotist",
                    "https://static.wikia.nocookie.net/town-of-salem/images/1/18/RoleIcon_Hypnotist.png/revision/latest/scale-to-width-down/100?cb=20200910205813",
                    "https://static.wikia.nocookie.net/town-of-salem/images/1/18/RoleIcon_Hypnotist.png/revision/latest/scale-to-width-down/100?cb=20200910205813"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/e/e8/Hypnotist_Skin.png/revision/latest/scale-to-width-down/150?cb=20200319032815")
                builder.setDescription("Plant a fake memory in the mind of a player. They will receive a fake result of your choosing.")
                builder.setColor(mafiaColor)
            }
            "godfather" -> {

                builder.addField("Class", "Mafia Killing (MK)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "The leader of organized crime.", false)
                builder.setAuthor(
                    "Godfather",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/b/b1/Achievement_Godfather.png/revision/latest/scale-to-width-down/50?cb=20140825150541"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/9/97/Godfather_2.png/revision/latest/scale-to-width-down/100?cb=20160606042812")
                builder.setDescription("You decide who the mafia will kill. If you are role blocked, the Mafioso's kill will be used instead of your own. You are unable to be found by a Sheriff.")

                builder.setColor(mafiaColor)
            }
            "mafioso" -> {

                builder.addField("Class", "Mafia Killing (MK)", true)
                builder.addField("Goal", "Kill anyone who doesn't submit to Mafia.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Your allies are Survivors, Pirates, Guardian Angels, Witches, and other Mafia Members.",
                    false
                )
                builder.addField("Summary", "A member of organized crime, trying to work their way to the top.", false)
                builder.setAuthor(
                    "Mafioso",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/7/70/Achievement_Mafioso.png/revision/latest/scale-to-width-down/50?cb=20140825150640"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/7/70/DarkRevenant.png/revision/latest/scale-to-width-down/87?cb=20140701002425")
                builder.setDescription("You may choose a target to attack. If the Godfather chooses his own, then you will attack the Godfather's target instead. If the Godfather dies, you will be promoted to Godfather.")

                builder.setColor(mafiaColor)
            }
            "amnesiac" -> {

                builder.addField("Class", "Neutral Benign (NB)", true)
                builder.addField("Goal", "Remember a role and complete that role's goal.", true)
                builder.addField("Attack: 0 - Defence: 0", "You have no allies until you remember a role.", false)
                builder.addField("Summary", "A trauma patient that does not remember who he was.", false)
                builder.setAuthor(
                    "Amnesiac",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/6/65/Achievement_Amnesiac.png/revision/latest/scale-to-width-down/50?cb=20140825150322"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/2/2f/Forgetful_Freddy.png/revision/latest/scale-to-width-down/110?cb=20160826030733")
                builder.setDescription("Remember who you were by selecting a dead person's role to remember.")
                builder.setColor(amneColor)

            }
            "survivor" -> {

                builder.addField("Class", "Neutral Benign (NB)", true)
                builder.addField("Goal", "Survive to the end of the game.", true)
                builder.addField("Attack: 0 - Defence: 0", "Everyone is your ally if they let you survive.", false)
                builder.addField("Summary", "A Neutral character who just wants to live.", false)
                builder.setAuthor(
                    "Survivor",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/5/57/Achievement_Survivor.png/revision/latest/scale-to-width-down/50?cb=20140825150726"
                )
                builder.setThumbnail("https://orig00.deviantart.net/8843/f/2016/008/a/1/survivor_avatar_for_tos_by_nasubinori-d9n6u8b.png")
                builder.setDescription("You may use a vest protecting yourself. Each of 4 vests can only be used once.")
                builder.setColor(survColor)

            }
            "executioner" -> {

                builder.addField("Class", "Neutral Evil (NE)", true)
                builder.addField("Goal", "Get your target lynched", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Everyone can be your ally as long as your goal is completed.",
                    false
                )
                builder.addField(
                    "Summary",
                    "An obsessed lyncher who will stop at nothing to execute his target.",
                    false
                )
                builder.setAuthor(
                    "Executioner",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/8/8c/Achievement_Executioner.png/revision/latest/scale-to-width-down/50?cb=20140825150517"
                )
                builder.setThumbnail("https://orig00.deviantart.net/8843/f/2016/008/a/1/survivor_avatar_for_tos_by_nasubinori-d9n6u8b.png")
                builder.setDescription("Trick the Town into lynching your target. If your target gets killed by anyone else, you will become a Jester.")
                builder.setColor(Color.LIGHT_GRAY)

            }
            "jester" -> {

                builder.addField("Class", "Neutral Evil (NE)", true)
                builder.addField("Goal", "Get lynched by the town", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Everyone can be your ally as long as your goal is completed.",
                    false
                )
                builder.addField("Summary", "A crazed lunatic whose life goal is to be publicly executed.", false)
                builder.setAuthor(
                    "Jester",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/0/05/Achievement_Jester.png/revision/latest/scale-to-width-down/50?cb=20140825150623"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/e/e0/Jester.png/revision/latest?cb=20140716035330")
                builder.setDescription("Trick the town into lynching you.")
                builder.setColor(Color.PINK)

            }
            "witch" -> {

                builder.addField("Class", "Neutral Evil (NE)", true)
                builder.addField("Goal", "Survive to see the Town lose the game.", true)
                builder.addField("Attack: 0 - Defence: 0", "Everyone but the town can be your ally", false)
                builder.addField("Summary", "A voodoo master who can control other people's actions.", false)
                builder.setAuthor(
                    "Witch",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/2/20/Achievement_Witch.png/revision/latest/scale-to-width-down/50?cb=20140825150816"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/e/e6/Witch.png/revision/latest?cb=20140716035354")
                builder.setDescription("Control someone each night. You can force people to target themselves. Your victim will know they are being controlled. You will know the role of the player you control.")
                builder.setColor(witchColor)

            }
            "arsonist" -> {

                builder.addField("Class", "Neutral Killing (NK)", true)
                builder.addField("Goal", "See everyone burn.", true)
                builder.addField(
                    "Attack: 3 - Defence: 1",
                    "Survivor, Witch, Pirate, Guardian Angels, and other Arsonists are your allies.",
                    false
                )
                builder.addField("Summary", "A pyromaniac that wants to burn everyone.", false)
                builder.setAuthor(
                    "Arsonist",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/c/cf/Achievement_Arsonist.png/revision/latest/scale-to-width-down/50?cb=20140825150335"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/c/c3/Arsonist.png/revision/latest?cb=20141029221117")
                builder.setDescription("You may Douse someone in gasoline or ignite Doused targets. Players will also be doused if they visit you. If you are doused and do no action one night, you will clean off the gas from another Arsonist.")
                builder.setColor(arsoColor)

            }
            "serial_killer" -> {

                builder.addField("Class", "Neutral Killing (NK)", true)
                builder.addField("Goal", "Kill everyone who would oppose you.", true)
                builder.addField(
                    "Attack: 1 - Defence: 1",
                    "Survivor, Witch, Pirate, Guardian Angels, and other Serial Killers are your allies.",
                    false
                )
                builder.addField("Summary", "A psychotic criminal who wants everyone to die.", false)
                builder.setAuthor(
                    "Serial Killer",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/9/98/Achievement_Serial_Killer.png/revision/latest/scale-to-width-down/50?cb=20140723234035"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/7/75/SerialKiller.png/revision/latest?cb=20140816021322")
                builder.setDescription("You may kill a target every night. If someone tries to roleblock you, they will die. You may choose to not kill roleblockers by using your cautious ability. If you are Jailed and not executed, you will kill the Jailor.")
                builder.setColor(skColor)

            }
            "werewolf" -> {

                builder.addField("Class", "Neutral Killing (NK)", true)
                builder.addField("Goal", "Kill everyone who would oppose you.", true)
                builder.addField(
                    "Attack: 2 - Defence: 1",
                    "Survivor, Witch, Pirate, Guardian Angels, and other Werewolves are your allies.",
                    false
                )
                builder.addField("Summary", "A normal citizen who transforms during the full moon.", false)
                builder.setAuthor(
                    "Werewolf",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/0/07/Achievement_Werewolf2.png/revision/latest?cb=20170730212305"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/c/c1/Lycanthrope.png/revision/latest/scale-to-width-down/151?cb=20160506214350")
                builder.setDescription("You will go on a rampage every other night. After Night 4, you may attack every night. Going on a rampage means that all people who visit your target will die. If you do not chose a target, then all people who visit you will die")
                builder.setColor(wwColor)
            }
            "vampire" -> {
                builder.addField("Class", "Neutral Chaos (NC)", true)
                builder.addField("Goal", "Convert everyone who would oppose you.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Survivor, Witch, Pirate, Guardian Angels, and other Vampires are your allies.",
                    false
                )
                builder.addField("Summary", "Someone among the undead who turns others at night.", false)
                builder.setAuthor(
                    "Vampire",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://vignette.wikia.nocookie.net/town-of-salem/images/7/75/Achievement_Vampire.png/revision/latest/scale-to-width-down/50?cb=20151130211326"
                )
                builder.setThumbnail("https://vignette.wikia.nocookie.net/town-of-salem/images/4/4e/Vampire.png/revision/latest/scale-to-width-down/150?cb=20151101133009")
                builder.setDescription("Every other night you may bite someone who is not already a Vampire. If this person is a Vampire Hunter, you will die. If this person is a Mafia Member, they will die. Your bite does not go through Night Immunity.")
                builder.setColor(vampColor)
            }
            "coven_leader" -> {
                builder.addField("Class", "Coven Evil (CE)", true)
                builder.addField("Goal", "Kill all who would oppose the Coven.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Survivor, Pirate, Guardian Angels, Executioner, and other Coven members are your allies.",
                    false
                )
                builder.addField("Summary", "You are a voodoo master who can control other peoples actions.", false)
                builder.setAuthor(
                    "Coven Leader",
                    "https://town-of-salem.fandom.com/wiki/Coven_Leader",
                    "https://static.wikia.nocookie.net/town-of-salem/images/b/b2/RoleIcon_CovenLeader.png/revision/latest/scale-to-width-down/100?cb=20200910205232"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/7/77/Coven_Leader_Skin.png/revision/latest/scale-to-width-down/150?cb=20181221194132")
                builder.setDescription("You may choose to control someone every night. You will recieve the feedback that your controlled person would have recieved. With the Necronomicon, you will gain a Basic Attack(1) and Basic Defence(1).")
                builder.setColor(witchColor)
            }
            "hex_master" -> {
                builder.addField("Class", "Coven Evil (CE)", true)
                builder.addField("Goal", "Kill all who would oppose the Coven.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Survivor, Pirate, Guardian Angels, Executioner, and other Coven members are your allies.",
                    false
                )
                builder.addField("Summary", "You are a spell slinger with a proficiency for hexes and curses.", false)
                builder.setAuthor(
                    "Hex Master",
                    "https://town-of-salem.fandom.com/wiki/Hex_Master",
                    "https://static.wikia.nocookie.net/town-of-salem/images/b/bd/RoleIcon_HexMaster.png/revision/latest/scale-to-width-down/100?cb=20200910205233"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/5/53/Hex_Master_Avatar.png/revision/latest/scale-to-width-down/150?cb=20190329023512")
                builder.setDescription("Choose a person to Hex every night. Once everyone alive is Hexed, they will all be delt an Unstoppable(3) attack. With the necronomicon, you gain a Basic(1) attack every night.")
                builder.setColor(witchColor)
            }
            "poisoner" -> {
                builder.addField("Class", "Coven Evil (CE)", true)
                builder.addField("Goal", "Kill all who would oppose the Coven.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Survivor, Pirate, Guardian Angels, Executioner, and other Coven members are your allies.",
                    false
                )
                builder.addField("Summary", "You are a patient killer with a knowledge of poisons.", false)
                builder.setAuthor(
                    "Poisoner",
                    "https://town-of-salem.fandom.com/wiki/Poisoner",
                    "https://static.wikia.nocookie.net/town-of-salem/images/e/e8/RoleIcon_Poisoner.png/revision/latest/scale-to-width-down/100?cb=20200910205234"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/8/86/Poisoner_Avatar.png/revision/latest/scale-to-width-down/150?cb=20171102222135")
                builder.setDescription("Choose a person to poison every night. You are unable to poison those who are immune. With the Necronomicon, your poison cannot be healed.")
                builder.setColor(witchColor)
            }
            "potion_master" -> {
                builder.addField("Class", "Coven Evil (CE)", true)
                builder.addField("Goal", "Kill all who would oppose the Coven.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Survivor, Pirate, Guardian Angels, Executioner, and other Coven members are your allies.",
                    false
                )
                builder.addField("Summary", "You are an experienced alchemist with potent recipes for potions.", false)
                builder.setAuthor(
                    "Potion Master",
                    "https://town-of-salem.fandom.com/wiki/Potion_Master",
                    "https://static.wikia.nocookie.net/town-of-salem/images/f/f1/RoleIcon_PotionMaster.png/revision/latest/scale-to-width-down/100?cb=20200910205235"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/1/1b/PotionMaster.png/revision/latest/scale-to-width-down/150?cb=20181225221505")
                builder.setDescription("You have 3 potions, Kill, Reveal, and Heal. Each of these potions have a 3 night cooldown. With the necronomicon, your potions will not have a cooldown")
                builder.setColor(witchColor)
            }
            "medusa" -> {
                builder.addField("Class", "Coven Evil (CE)", true)
                builder.addField("Goal", "Kill all who would oppose the Coven.", true)
                builder.addField(
                    "Attack: 2 - Defence: 0",
                    "Survivor, Pirate, Guardian Angels, Executioner, and other Coven members are your allies.",
                    false
                )
                builder.addField("Summary", "You are a snake haired monster with a gaze of stone.", false)
                builder.setAuthor(
                    "Medusa",
                    "https://town-of-salem.fandom.com/wiki/Medusa",
                    "https://static.wikia.nocookie.net/town-of-salem/images/c/c1/RoleIcon_Medusa.png/revision/latest/scale-to-width-down/100?cb=20200910205233"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/b/b7/Medusa_Avatar.png/revision/latest/scale-to-width-down/200?cb=20171102222152")
                builder.setDescription("Every night you may choose to stonegaze. Similar to Veteran, you only have 3 uses of your stonegaze. With the Necronomicon, you may visit people and stonegaze them with no limit.")
                builder.setColor(witchColor)
            }
            "necromancer" -> {
                builder.addField("Class", "Coven Evil (CE)", true)
                builder.addField("Goal", "Kill all who would oppose the Coven.", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Survivor, Pirate, Guardian Angels, Executioner, and other Coven members are your allies.",
                    false
                )
                builder.addField("Summary", "You are an evil sorceress who has a grudge against the town.", false)
                builder.setAuthor(
                    "Necromancer",
                    "https://town-of-salem.fandom.com/wiki/Necromancer",
                    "https://static.wikia.nocookie.net/town-of-salem/images/0/03/RoleIcon_Necromancer.png/revision/latest/scale-to-width-down/100?cb=20200910205234"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/d/db/Necromancer_Avatar.png/revision/latest/scale-to-width-down/150?cb=20180222144528")
                builder.setDescription("You may reanimate each of the dead 1 time (see FAQ for more details). With the necronomicon, you can choose yourself to summon a Ghoul to attack someone.")
                builder.setColor(witchColor)
            }
            "crusader" -> {
                builder.addField("Class", "Town Protective (TP)", true)
                builder.addField("Goal", "Lynch ever criminal and evildoer.", true)
                builder.addField(
                    "Attack: 1 - Defence: 0",
                    "Survivor, Guardian Angels, and other Town members are your allies.",
                    false
                )
                builder.addField("Summary", "You are a divine protector whose skills in combat are unmatched.", false)
                builder.setAuthor(
                    "Crusader",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/7/78/RoleIcon_Crusader.png/revision/latest/scale-to-width-down/100?cb=20200910205848"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/e/ee/Crusader_Avatar.png/revision/latest/scale-to-width-down/150?cb=20170731083617")
                builder.setDescription("You may choose to visit someone and defend them. You will attack one person who visits them.")
                builder.setColor(townColor)
            }
            "guardian_angel" -> {
                builder.addField("Class", "Neutral Benign (NB)", true)
                builder.addField("Goal", "Keep your target alive until the end of the game", true)
                builder.addField("Attack: 0 - Defence: 0", "Anyone who won't oppose your goal", false)
                builder.addField("Summary", "You are an Angel whose only goal is the protection of your charge.", false)
                builder.setAuthor(
                    "Guardian Angel",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/b/b3/RoleIcon_GuardianAngel.png/revision/latest/scale-to-width-down/100?cb=20200910205950"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/a/a7/Uriel.png/revision/latest/scale-to-width-down/150?cb=20160625010538")
                builder.setDescription("Twice, you may choose to Heal and Purge your target. This will prevent them from being voted the next day as well as prevent them from being executed.")
                builder.setColor(gaColor)
            }
            "juggernaut" -> {
                builder.addField("Class", "Neutral Chaos (NC)", true)
                builder.addField("Goal", "Kill everyone who would oppose you", true)
                builder.addField(
                    "Attack: 2 - Defence: 1",
                    "Survivor, Guardian Angels, and Executioners are your allies",
                    false
                )
                builder.addField(
                    "Summary",
                    "You are an unstoppable force that only gets stronger with each kill.",
                    false
                )
                builder.setAuthor(
                    "Juggernaut",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/d/d9/RoleIcon_Juggernaut.png/revision/latest?cb=20200910205950"
                )
                builder.setThumbnail("")
                builder.setDescription("On the full moons, you can attack a player. After your first kill, you may kill every night. After the second kill, you will rampage at your target's house. After the third, you ignore everything that may protect a player. (Attack of 3)")
                builder.setColor(juggColor)
            }
            "pestilence" -> {
                builder.addField("Class", "Neutral Chaos (NC)", true)
                builder.addField("Goal", "Kill everyone who would oppose you", true)
                builder.addField(
                    "Attack: 2 - Defence: 3",
                    "Survivor, Guardian Angels, and Executioners are your allies",
                    false
                )
                builder.addField("Summary", "You are a horseman of the apocalypse who feeds off of disease.", false)
                builder.setAuthor(
                    "Pestilence, Horseman of the Apocalypse",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/7/75/RoleIcon_PlagueBearer_2.png/revision/latest/scale-to-width-down/100?cb=20200910205952"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/7/79/Plaguebearer_Avatar.png/revision/latest/scale-to-width-down/150?cb=20171102222205")
                builder.setDescription("Choose a player's house to rampage at. If you do not choose a target, you will rampage at your own house. Jailor cannot stop you as you cannot be executed and will kill the Jailor.")
                builder.setColor(pestColor)
            }
            "pirate" -> {
                builder.addField("Class", "Neutral Chaos (NC)", true)
                builder.addField("Goal", "Kill everyone who would oppose you", true)
                builder.addField(
                    "Attack: 2 - Defence: 0",
                    "Anyone who will not stop you from achieving your goal",
                    false
                )
                builder.addField("Summary", "You are a swashbuckler with an obsession for gold.", false)
                builder.setAuthor(
                    "Pirate",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/2/2a/RoleIcon_Pirate.png/revision/latest/scale-to-width-down/100?cb=20200910205951"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/7/7d/Pirate.png/revision/latest/scale-to-width-down/177?cb=20150725220303")
                builder.setDescription("Choose a target to plunder. They will be roleblocked for that night.")
                builder.setColor(pirateColor)
            }
            "plaguebearer" -> {
                builder.addField("Class", "Neutral Chaos (NC)", true)
                builder.addField("Goal", "Kill everyone who would oppose you", true)
                builder.addField(
                    "Attack: 0 - Defence: 1",
                    "Survivor, Guardian Angels, and Executioners are your allies",
                    false
                )
                builder.addField(
                    "Summary",
                    "You are an acolyte of Pestilence who spreads disease among the town.",
                    false
                )
                builder.setAuthor(
                    "Plaguebearer",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/f/f6/RoleIcon_PlagueBearer_1.png/revision/latest?cb=20200910205951"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/7/79/Plaguebearer_Avatar.png/revision/latest/scale-to-width-down/150?cb=20171102222205")
                builder.setDescription("Choose a target to infect. You will infect them and all who visit them. Once all players are infected, you will become Pestilence, Horseman of the Apocalypse")
                builder.setColor(plagueColor)
            }
            "tracker" -> {
                builder.addField("Class", "Town Investigative (TI)", true)
                builder.addField("Goal", "Lynch every criminal and evildoer", true)
                builder.addField(
                    "Attack: 0 - Defence: 0",
                    "Survivor, Guardian Angels, and other Town members are your allies",
                    false
                )
                builder.addField(
                    "Summary",
                    "You are a skilled tracker who will follow their prey to any destination.",
                    false
                )
                builder.setAuthor(
                    "Tracker",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/1/12/RoleIcon_Tracker.png/revision/latest/scale-to-width-down/100?cb=20200910205850"
                )
                builder.setThumbnail("https://static.wikia.nocookie.net/town-of-salem/images/0/00/Tracker_Skin.png/revision/latest/scale-to-width-down/150?cb=20181221193213")
                builder.setDescription("Choose a target to follow. You will see who they visit.")
                builder.setColor(townColor)
            }
            "trapper" -> {
                builder.addField("Class", "Town Protective (TP)", true)
                builder.addField("Goal", "Lynch every criminal and evildoer", true)
                builder.addField(
                    "Attack: 2 - Defence: 0",
                    "Survivor, Guardian Angels, and other Town members are your allies",
                    false
                )
                builder.addField("Summary", "You are an intelligent woodsman with a knack for traps.", false)
                builder.setAuthor(
                    "Trapper",
                    "https://town-of-salem.fandom.com/wiki/$role",
                    "https://static.wikia.nocookie.net/town-of-salem/images/d/d8/RoleIcon_Trapper.png/revision/latest/scale-to-width-down/100?cb=20200910205850"
                )
                builder.setThumbnail("")
                builder.setDescription("When you do not have a trap, you will build a trap at night. Once built, you may place the trap on a player. You may only have 1 trap out at a time, and may tear down your trap, although it will take a night to do so.")
                builder.setColor(townColor)
            }
            else -> {
                if (role == "pest") {
                    return onRoleCardAsk(message, "pestilence", user);
                } else if (role == "vig" || role == "vigi") {
                    return onRoleCardAsk(message, "vigilante", user)
                } else if (role == "vet") {
                    return onRoleCardAsk(message, "veteran", user)
                } else if (role == "med") {
                    return onRoleCardAsk(message, "medium", user)
                } else if (role == "ret") {
                    return onRoleCardAsk(message, "retributionist", user)
                } else if(role == "dusa"){
                    return onRoleCardAsk(message, "medusa", user)
                }
            }
        }
        return builder.build()
    }
}