package maxdistructo.tosbot

import net.dv8tion.jda.api.entities.Message

object Utils {
    fun getArgs(message: Message) : List<String>
    {
        val content = message.contentRaw.split(" ")
        val args = mutableListOf<String>()
        try {
            if (content[2].toLowerCase() == "hunter" || content[2].toLowerCase() == "killer" || content[2].toLowerCase() == "leader" || content[2].toLowerCase() == "master" || content[2].toLowerCase() == "angel") {
                args += content[0]
                args += content[1].toLowerCase() + "_" + content[2].toLowerCase()
                try {
                    args += content[3]
                } catch (e: IndexOutOfBoundsException) {
                }
                return args
            }
        }
        catch(e: IndexOutOfBoundsException)
        {}
        return content
    }
    fun fixName(string: String): String
    {
        var fixedName = string
        fixedName = fixedName.replace("_"," ")

        val charArrayName = fixedName.toCharArray()
        if(fixedName.contains(" ")) {
            charArrayName[0] = charArrayName[0].toUpperCase()
            charArrayName[fixedName.indexOf(' ')] = charArrayName[fixedName.indexOf(' ')].toUpperCase()
        }
        else{
            charArrayName[0] = charArrayName[0].toUpperCase()
        }
        return charArrayName.concatToString()
    }
}