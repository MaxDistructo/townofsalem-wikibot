package maxdistructo.tosbot

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.jda.Config
import maxdistructo.discord.core.jda.impl.Bot
import maxdistructo.tosbot.legacy.FAQs
import maxdistructo.tosbot.legacy.InvestResults
import maxdistructo.tosbot.legacy.RoleCards
import org.slf4j.Logger

object ToSBot{
    var bot: Bot = Bot(Config.readToken(), Config.readPrefix(), "228111371965956099")
    lateinit var logger: Logger

    @JvmStatic
    fun main(args: Array<String>){
        bot.registerCommands(CommandPing(), RoleCards.Command(), InvestResults.Command(), FAQs.Command())
        bot.init()
        logger = bot.logger
    }

    class CommandPing : Command() {
        init{
            this.help = "Basic Ping/Pong. Tests if the bot is working."
            this.guildOnly = false
            this.name = "ping"
        }
        override fun execute(event: CommandEvent?) {
            event?.reply("Pong!")
        }

    }
}