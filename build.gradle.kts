plugins {
    java
    idea
    maven
    kotlin("jvm") version "1.4.10"
}

group = "maxdistructo.tosbot"
version = "1.0"

repositories {
    mavenCentral()
    jcenter()
    maven{
        name = "Jitpack"
        url = uri("https://jitpack.io")
    }
}

dependencies {
    implementation(group = "com.gitlab.MaxDistructo", name = "mdCore-JDA", version="1.9.4")
    implementation(kotlin("stdlib"))
    implementation(group = "org.jsoup", name="jsoup", version="1.13.1")
    //testCompile("junit", "junit", "4.12")
}
